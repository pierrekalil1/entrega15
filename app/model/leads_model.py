from sqlalchemy import Column, Integer, String, DateTime
from app.configs.database import db
import datetime

class LeadsModel(db.Model):
    data_atual = datetime.datetime.utcnow()

    __tablename__ = "leads_control"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=True)
    email = Column(String, unique=True, nullable=True)
    phone = Column(String, unique=True, nullable=True)
    creation_date = Column(DateTime, nullable=True, default=data_atual.strftime(f'%d/%m/%Y %H:%M:%S'))
    last_visit = Column(DateTime,  nullable=True, default=data_atual.strftime(f'%d/%m/%Y %H:%M:%S'))
    visits = Column(Integer, nullable=True, default=1)

    

    
