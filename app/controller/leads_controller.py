from flask import request, current_app, jsonify
from app.model.leads_model import LeadsModel
import re
import datetime

def create_lead():
    try:
        data = request.get_json()

        if type(data['name']) != str or type(data['email']) != str or type(data['phone']) != str:
                return {"erro": "Numeric data is invalid. Text field only"}, 400

        validate_phone = re.fullmatch(r"^(\([0-9]{2}\)[0-9]{5}-)[0-9]{4}$", data['phone'])
        if validate_phone == None:
            return jsonify({"error": "Invalid phone number format. Correct example (xx)xxxxx-xxxx"})
        
        leads = (
            LeadsModel
            .query
            .order_by(LeadsModel.visits.desc())
            .all()
        )

        seralizer = [
            {
                "name": lead.name,
                "email": lead.email,
                "phone": lead.phone,
                "creation_date": lead.creation_date,
                "last_visit": lead.last_visit,
                "visits": lead.visits
            }for lead in leads
        ]

        list_register = list()
        for value in seralizer:
            for v in value.values():
                list_register.append(v)

        if data['email'] in list_register or data['phone'] in list_register :
            return {"error": f"Email or Phone aleary exists"}, 409
        

        lead = LeadsModel(**data)

    
        current_app.db.session.add(lead)
        current_app.db.session.commit()

        return {
        "name": lead.name,
        "email": lead.email,
        "phone": lead.phone,
        "creation_date": lead.creation_date,
        "last_visit": lead.last_visit,
        "visits": lead.visits
        }
    except KeyError as e:
        return {"erro": f"Invalid field {e}"}, 400


def get_all():
    leads = (
        LeadsModel
        .query
        .order_by(LeadsModel.visits.desc())
        .all()
    )

    seralizer = [
        {
            "name": lead.name,
            "email": lead.email,
            "phone": lead.phone,
            "creation_date": lead.creation_date,
            "last_visit": lead.last_visit,
            "visits": lead.visits
        }for lead in leads
    ]

    if len(seralizer) > 0:
        return {"leads": seralizer}
    else:
        return {"msg": "No data found"}


def update_lead():
    data_atual = datetime.datetime.utcnow()
    data = request.get_json()

    for key in data.keys():
        if key != 'email':
            return {"erro": "Invalid fields"}
    
    if type(data['email']) != str:
            return {"erro": "Numeric data is invalid. Text field only"}, 400
    
    '''
    Buscando todos os registros para verificar se o email existe no database
    '''
    query = (
        LeadsModel
        .query
        .all()
        )
    seralizer = [
        {
            "email": visited.email,
        }for visited in query
    ]   
    list_register = list()
    for value in seralizer:
        for v in value.values():
            list_register.append(v)

    if data['email'] not in list_register :
        return {"error": f"Email not found"}, 409
    '''
    Final da pesquisa e verificação do email no database
    '''
    
    lead = (
        LeadsModel
        .query
        .filter_by(email=data['email'])
        .first_or_404()
        )
    
    setattr(lead, 'visits', lead.visits + 1)
    setattr(lead, 'last_visit', data_atual.strftime(f'%d/%m/%Y %H:%M:%S'))

    current_app.db.session.add(lead)
    current_app.db.session.commit()

    return {}, 204
    

def delete_lead():
    try:
        data = request.get_json()
        print(data)

        for key in data.keys():
            if key != 'email':
                return {"erro": "Invalid fields. Correct example 'email': "}
        
        if type(data['email']) != str:
            return {"erro": "Numeric data is invalid. Text field only"}, 400

        '''
        Buscando todos os registros para verificar se o email existe no database
        '''
        query = (
            LeadsModel
            .query
            .all()
            )
        seralizer = [
            {
                "email": visited.email,
            }for visited in query
        ]   
        list_register = list()
        for value in seralizer:
            for v in value.values():
                list_register.append(v)

        if data['email'] not in list_register :
            return {"error": f"Email not found"}, 409
        '''
        Final da pesquisa e verificação do email no database
        '''
        
        lead = (
            LeadsModel
            .query
            .filter_by(email=data['email'])
            .first_or_404()
            )
            
        
        current_app.db.session.delete(lead)
        current_app.db.session.commit()

        return {}, 204
    except KeyError as e:
        return {"erro": f"Invalid field {e}"}

