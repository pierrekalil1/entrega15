from flask import Blueprint
from app.controller.leads_controller import create_lead, delete_lead, get_all, update_lead

bp_lead = Blueprint("bp_lead", __name__, url_prefix="/leads")

bp_lead.post("")(create_lead)
bp_lead.get("")(get_all)
bp_lead.delete("")(delete_lead)
bp_lead.patch("")(update_lead)